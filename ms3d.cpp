// Minimal Milkshape3D Library
// Public domain
// by Gabor Szauer

#define MS3D_ENABLE_TEST        1

#ifndef _H_MS3D_
#define _H_MS3D_

#include <assert.h>
#include <fstream>
#include <iostream>
#if __linux
#include <GL/gl.h>
#include <GL/glut.h>
#else // TODO: OTHER PLATFORM HEADERS
#include <GLUT/glut.h> // Osx only :(
#endif

#if MS3D_ENABLE_TEST
#include "glm/glm.hpp"
#include "glm/ext.hpp"
#include "stb_image.h"
#endif

// TODO: DO WE REALLY WANT EVERYTHING IN HERE AS STATIC?? (NO)

#define MS3D_NEW(t)		(t*)malloc(sizeof(t));
#define MS3D_FREE(x)	if (x != 0) free(x); x = 0;
#define MS3D_READ1(x)   input.read((char*)&(x), 1)
#define MS3D_READ2(x)   input.read((char*)&(x), 2)
#define MS3D_READ4(x)   input.read((char*)&(x), 4)
#define MS3D_READ6(x)   input.read((char*)&(x), 6)
#define MS3D_READ10(x)  input.read((char*)&(x), 10)
#define MS3D_READ12(x)  input.read((char*)&(x), 12)
#define MS3D_READ16(x)  input.read((char*)&(x), 16)
#define MS3D_READ32(x)  input.read((char*)&(x), 32)
#define MS3D_READ36(x)  input.read((char*)&(x), 36)
#define MS3D_READ128(x) input.read((char*)&(x), 128)

#define MS3D_MAKE_TRANSFORM(outMatrix, inPosition, inRotation) ; // TODO!

typedef unsigned int 	(*ms3d_fp_loadtexture)(const char* texture);
typedef unsigned char   ms3d_ubyte;         // 1 byte
typedef char            ms3d_sbyte;         // 1 byte
typedef unsigned short  ms3d_uword;         // 2 bytes
typedef short           ms3d_sword;         // 2 bytes
typedef unsigned int    ms3d_udword;        // 4 bytes
typedef int             ms3d_sdword;        // 4 bytes
typedef float           ms3d_single;        // 4 bytes
typedef ms3d_sbyte		ms3d_string[32];	// 32 byte string
typedef ms3d_single		ms3d_touple[3];		// 12 byte floats

static ms3d_fp_loadtexture ms3d_loadtexture;

struct ms3d_material {
	ms3d_single ambient[4];
	ms3d_single diffuse[4];
	ms3d_single specular[4];
	ms3d_single emissive[4];
	ms3d_single shininess;
	ms3d_udword texture;
};

struct ms3d_mesh { 
	ms3d_single* vertices;
	ms3d_single* normals;
	ms3d_single* texcoords;
	ms3d_udword size;            // Number of vertices

	ms3d_material* materials;	// Array of materials
	ms3d_udword msize;			// Number of materials
	ms3d_udword* material;		// Array of material ids

	ms3d_udword** indices;		// Array if vertex arrays
	ms3d_udword* vsize;			// Size of each vertex array
	ms3d_udword isize;			// Size of overall vertex array
};

struct ms3d_frame {
	ms3d_single time;
	ms3d_single transform[16];
};

struct ms3d_bone {
	int parent;
	ms3d_single transform[16];
	ms3d_frame* track;
	ms3d_udword size;
};

struct ms3d_animation { 
	ms3d_single length;
	ms3d_bone* skeleton;
	int* skin; // index: vertex id, value: bone id
	ms3d_udword size;
};

struct ms3d_animpack {
	ms3d_single animTime;
    ms3d_touple animData;
};

static bool ms3d_load(const char* path, ms3d_mesh** mesh, ms3d_animation** animation) {
	// TODO: CHECK FOR NULL ARGUMENTS (animation can be null, if mesh isn't)
	assert(sizeof(ms3d_ubyte) == 1);
	assert(sizeof(ms3d_sbyte) == 1);
	assert(sizeof(ms3d_uword) == 2);
	assert(sizeof(ms3d_sword) == 2);
	assert(sizeof(ms3d_udword) == 4);
	assert(sizeof(ms3d_sdword) == 4);
	assert(sizeof(ms3d_single) == 4);

	std::ifstream input;
	input.open(path, std::ios::in | std::ios::binary);
	if (!input) {
		*mesh = 0;
		*animation = 0;
		return false;
	}

	ms3d_sbyte identifier[10]; 
	ms3d_udword version;
	MS3D_READ10(identifier[0]);
	MS3D_READ4(version);
	ms3d_uword numVerts; 
	MS3D_READ2(numVerts);

	*mesh = MS3D_NEW(ms3d_mesh);
	*animation = MS3D_NEW(ms3d_animation);
   
	(*mesh)->vertices = (ms3d_single*)malloc(sizeof(ms3d_single) * numVerts * 3);
	(*mesh)->normals = (ms3d_single*)malloc(sizeof(ms3d_single) * numVerts * 3);
	(*mesh)->texcoords = (ms3d_single*)malloc(sizeof(ms3d_single) * numVerts * 2);
	(*mesh)->size = numVerts;

	(*animation)->skin = (int*)malloc(sizeof(int) * numVerts);
	for (int i = 0, j = 0; i < int(numVerts); ++i) {
		ms3d_ubyte trash;
		MS3D_READ1(trash);
		ms3d_single position[3];
		MS3D_READ12(position[0]);
		(*mesh)->vertices[j + 0] = position[0];
		(*mesh)->vertices[j + 1] = position[1];
		(*mesh)->vertices[j + 2] = position[2];
		ms3d_ubyte skin;
		MS3D_READ1(skin);
		(*animation)->skin[i] = skin;
		MS3D_READ1(trash);

		j += 3;
	}

	ms3d_uword numTriangles; 
	MS3D_READ2(numTriangles);
	ms3d_udword* indexBuffer = (ms3d_udword*)malloc(sizeof(ms3d_udword) * numTriangles * 3);
	for (int i = 0, j = 0; i < int(numTriangles); ++i) {
		ms3d_uword trash;
		ms3d_uword verts[3];
		MS3D_READ2(trash);
		MS3D_READ6(verts[0]);
		indexBuffer[j + 0] = verts[0];
		indexBuffer[j + 1] = verts[1];
		indexBuffer[j + 2] = verts[2];

		ms3d_single normal[3];
		MS3D_READ12(normal[0]);
		(*mesh)->normals[verts[0] * 3 + 0] = normal[0];
		(*mesh)->normals[verts[0] * 3 + 1] = normal[1];
		(*mesh)->normals[verts[0] * 3 + 2] = normal[2];
		MS3D_READ12(normal[0]);
		(*mesh)->normals[verts[1] * 3 + 0] = normal[0];
		(*mesh)->normals[verts[1] * 3 + 1] = normal[1];
		(*mesh)->normals[verts[1] * 3 + 2] = normal[2];
		MS3D_READ12(normal[0]);
		(*mesh)->normals[verts[2] * 3 + 0] = normal[0];
		(*mesh)->normals[verts[2] * 3 + 1] = normal[1];
		(*mesh)->normals[verts[2] * 3 + 2] = normal[2];
		
		ms3d_single uv[3];
		MS3D_READ12(uv[0]);
		(*mesh)->texcoords[verts[0] * 2 + 0] = uv[0];
		(*mesh)->texcoords[verts[1] * 2 + 0] = uv[1];
		(*mesh)->texcoords[verts[2] * 2 + 0] = uv[2];
		MS3D_READ12(uv[0]);
		(*mesh)->texcoords[verts[0] * 2 + 1] = uv[0];
		(*mesh)->texcoords[verts[1] * 2 + 1] = uv[1];
		(*mesh)->texcoords[verts[2] * 2 + 1] = uv[2];

		MS3D_READ1(trash);
		MS3D_READ1(trash);

		j += 3;
	}

	ms3d_uword numGroups; 
	MS3D_READ2(numGroups);

	(*mesh)->isize = numGroups;
	(*mesh)->indices = (ms3d_udword**)malloc(sizeof(ms3d_udword*) * numGroups);
	(*mesh)->vsize = (ms3d_udword*)malloc(sizeof(ms3d_udword) * numGroups);
	(*mesh)->material = (ms3d_udword*)malloc(sizeof(ms3d_udword) * numGroups);
	for (int i = 0; i < int(numGroups); ++i) {
		ms3d_sbyte trash[32];

		MS3D_READ1(trash[0]);
		MS3D_READ32(trash[0]);
		ms3d_uword triCount;
		MS3D_READ2(triCount);
		(*mesh)->indices[i] = (ms3d_udword*)malloc(sizeof(ms3d_udword) * triCount * 3);
		(*mesh)->vsize[i] = triCount;

		ms3d_uword triIndex;
		for (int j = 0, k = 0; j < int(triCount); ++j) {
			MS3D_READ2(triIndex);
			(*mesh)->indices[i][k + 0] = indexBuffer[triIndex * 3 + 0];
			(*mesh)->indices[i][k + 1] = indexBuffer[triIndex * 3 + 1];
			(*mesh)->indices[i][k + 2] = indexBuffer[triIndex * 3 + 2];
			k += 3;
		}
		
		ms3d_ubyte materialIndex;
		MS3D_READ1(materialIndex);
		(*mesh)->material[i] = materialIndex;
	}

	ms3d_uword numMaterials; 
	MS3D_READ2(numMaterials);
	(*mesh)->msize = numMaterials;
	(*mesh)->materials = (ms3d_material*)malloc(sizeof(ms3d_material) * numMaterials);
	for (int i = 0; i < int(numMaterials); ++i) {
		ms3d_sbyte name[32];
		ms3d_single transperancy;
		ms3d_sbyte mode;
		ms3d_sbyte texture[128];
		(*mesh)->materials[i].texture = 0;

		MS3D_READ32(name[0]);
		MS3D_READ16((*mesh)->materials[i].ambient[0]);
		MS3D_READ16((*mesh)->materials[i].diffuse[0]);
		MS3D_READ16((*mesh)->materials[i].specular[0]);
		MS3D_READ16((*mesh)->materials[i].emissive[0]);
		MS3D_READ4((*mesh)->materials[i].shininess);
		MS3D_READ4(transperancy);
		MS3D_READ1(mode);
		MS3D_READ128(texture);
		if (texture[0] != '\0' && ms3d_loadtexture != 0)
			(*mesh)->materials[i].texture = ms3d_loadtexture(texture);
		MS3D_READ128(texture);
	}

	free(indexBuffer);

	ms3d_single fps;
	ms3d_single curtime;
	ms3d_sdword numFrames;
	ms3d_uword numJoints;

	MS3D_READ4(fps);
	MS3D_READ4(curtime);
	MS3D_READ4(numFrames);
	MS3D_READ2(numJoints);

	(*animation)->length = fps * numFrames;
	(*animation)->size = numJoints;

	(*animation)->skeleton = (ms3d_bone*)malloc(sizeof(ms3d_bone) * numJoints);
	ms3d_string* jointNames = (ms3d_string*)malloc(sizeof(ms3d_string) * numJoints);
	ms3d_string* parentNames = (ms3d_string*)malloc(sizeof(ms3d_string) * numJoints);
	memset(jointNames, 0, sizeof(ms3d_string) * numJoints);
	memset(parentNames, 0, sizeof(ms3d_string) * numJoints);

	for (int i = 0; i < int(numJoints); ++i) {
		ms3d_sdword trash;
		MS3D_READ1(trash);
		MS3D_READ32(jointNames[i][0]);
        MS3D_READ32(parentNames[i][0]);
        //std::cout << "\nJoint name: " << jointNames[i] << "\n";
        //std::cout << "\nParent name: " << parentNames[i] << "\n";

        ms3d_touple rotation;
        ms3d_touple translation;
       	MS3D_READ12(rotation[0]);
        MS3D_READ12(translation[0]);
        MS3D_MAKE_TRANSFORM((*animation)->skeleton[i].transform, translation, rotation);

        ms3d_uword numRotFrames;
        ms3d_uword numTransFrames;
        MS3D_READ2(numRotFrames);
        MS3D_READ2(numTransFrames);
        //std::cout << "Num rot frames: " << numRotFrames << "\n";
        //std::cout << "Num trans frames: " << numTransFrames << "\n";

        std::vector<ms3d_animpack> rotationFrames;
        rotationFrames.resize(numRotFrames);
        for (int i = 0; i < int(numRotFrames); ++i) {
        	MS3D_READ4(rotationFrames[i].rotationFrames[i].animTime);
            MS3D_READ12(rotationFrames[i].animData);
        }
        std::vector<ms3d_animpack> translationFrames;
        rotationFrames.resize(numTransFrames);
        for (int i = 0; i < int(numTransFrames); ++i) {
        	MS3D_READ4(translationFrames[i].animTime);
            MS3D_READ12(translationFrames[i].animData);
        }

        std::vector<ms3d_frame> animTrack = ms3d_resolve_dopesheet(rotationFrames, translationFrames);
        (*animation)->skeleton[i].track = (ms3d_frame*)malloc(sizeof(ms3d_frame) * animTrack.size());
        (*animation)->skeleton[i].size = animTrack.size();
        for (int i = 0; i < int(animTrack.size()); ++i) {
        	(*animation)->skeleton[i].track[i].time = animTrack[i].time;
        	memcpy((*animation)->skeleton[i].track[i].transform, animTrack[i].transform, sizeof(ms3d_single) * 16);
        }
	}

	// TODO: POST PROCESS SKELETON AND FREE ANY DANGLING DATA
	
	free(jointNames);
	free(parentNames);

	return true;
}

static std::vector<ms3d_frame> ms3d_resolve_dopesheet(std::vector<ms3d_animpack>& rotation, std::vector<ms3d_animpack>& translation) {
	// TODO
	// This needs to also calculate the matrices!
}

static void ms3d_unloadmesh(ms3d_mesh* mesh) {
	MS3D_FREE(mesh->vertices);
	MS3D_FREE(mesh->normals);
	MS3D_FREE(mesh->texcoords);
	for (int i = 0, isize = mesh->isize; i < isize; ++i) {
		if (mesh->materials[i].texture != 0) {
			glDeleteTextures(1, &mesh->materials[i].texture);
		}
		MS3D_FREE(mesh->indices[i]);
	}
	MS3D_FREE(mesh->vsize);
	MS3D_FREE(mesh->indices);
	MS3D_FREE(mesh->materials);
	MS3D_FREE(mesh->material);
	MS3D_FREE(mesh);
}

static void ms3d_unloadanimation(ms3d_animation* animation) {
	MS3D_FREE(animation->skin);
	for (int i = 0, isize = animation->size; i < isize; ++i) {
		MS3D_FREE(animation->skeleton[i].track);
	}
	MS3D_FREE(animation->skeleton);
	MS3D_FREE(animation);

	// TODO: CONFIRM!
}

static void ms3d_copymesh(const ms3d_mesh& src, ms3d_mesh* dst) { 
	if (dst == 0)
		return;

	dst->size = src.size;
	dst->vertices = (ms3d_single*)malloc(sizeof(ms3d_single) * src.size * 3);
	dst->normals = (ms3d_single*)malloc(sizeof(ms3d_single) * src.size * 3);
	dst->texcoords = (ms3d_single*)malloc(sizeof(ms3d_single) * src.size * 2);
	memcpy(dst->vertices, src.vertices, sizeof(ms3d_single) * src.size * 3);
	memcpy(dst->normals, src.normals, sizeof(ms3d_single) * src.size * 3);
	memcpy(dst->texcoords, src.texcoords, sizeof(ms3d_single) * src.size * 2);

	dst->msize = src.msize;
	dst->materials = (ms3d_material*)malloc(sizeof(ms3d_material) * src.msize);
	memcpy(dst->materials, src.materials, sizeof(ms3d_material) * src.msize);
	// TODO: BETTER HANDLE TEXTURES!!!!

	dst->material = (ms3d_udword*)malloc(sizeof(ms3d_udword) * src.isize);
	memcpy(dst->material, src.material, sizeof(ms3d_udword) * src.isize);

	dst->isize = src.isize;
	dst->vsize = (ms3d_udword*)malloc(sizeof(ms3d_udword) * src.isize);
	memcpy(dst->vsize, src.vsize, sizeof(ms3d_udword) * src.isize);

	dst->indices = (ms3d_udword**)malloc(sizeof(ms3d_udword*) * src.isize);
	for (int i = 0; i < int(src.isize); ++i) {
		dst->indices[i] = (ms3d_udword*)malloc(sizeof(ms3d_udword) * src.vsize[i] * 3);
		memcpy(dst->indices[i], src.indices[i], sizeof(ms3d_udword) * src.vsize[i] * 3);
	}
}

static void ms3d_copyanimation(const ms3d_animation& src, ms3d_animation* dst) { 
	// TODO
}

static ms3d_single ms3d_animate(ms3d_single time, int startFrame, int endFrame, const ms3d_mesh& source, ms3d_mesh& target, ms3d_animation& animation) {
	return 0.0f; // TODO
}

static void ms3d_render(const ms3d_mesh& mesh) { 
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, mesh.vertices);
	glNormalPointer(GL_FLOAT, 0, mesh.normals);
	glTexCoordPointer(2, GL_FLOAT, 0, mesh.texcoords);
	for (int i = 0; i < mesh.isize; ++i) {
		if (mesh.materials[mesh.material[i]].texture != 0)
			glBindTexture(GL_TEXTURE_2D, mesh.materials[mesh.material[i]].texture);

		glMaterialfv(GL_FRONT, GL_AMBIENT, mesh.materials[mesh.material[i]].ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, mesh.materials[mesh.material[i]].diffuse);
		glMaterialfv(GL_FRONT, GL_EMISSION, mesh.materials[mesh.material[i]].specular);
		glMaterialfv(GL_FRONT, GL_SPECULAR, mesh.materials[mesh.material[i]].emissive);
		glMaterialf(GL_FRONT, GL_SHININESS, mesh.materials[mesh.material[i]].shininess);

		glDrawElements(GL_TRIANGLES, mesh.vsize[i] * 3, GL_UNSIGNED_INT, &mesh.indices[i][0]);
	}
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}


#if MS3D_ENABLE_TEST
#include <stdlib.h>
#include <vector>

static ms3d_mesh* gMesh;
static ms3d_animation* gAnimation;

unsigned int LoadTexture(const char* path) {
	int bitdepth = 0;
	int width, height;

	unsigned char *data_raw = stbi_load(path, &width, &height, &bitdepth, 4);
	if (data_raw == 0) {
		std::cout << "Couldn't load texture: " << path << "\n";
		return 0;
	}
	
	unsigned int handle;
	glGenTextures(1, &handle);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, handle);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //GL_NEAREST = no smoothing
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid*)data_raw);
	
	stbi_image_free(data_raw);

	return handle;
}

void line(ms3d_single x, ms3d_single y, ms3d_single z) {
	glBegin(GL_LINES);
	glColor3f(x, y, z);
	glVertex3f(0.0f,0.0f,0.0f);
	glColor3f(x, y, z);
	glVertex3f(x, y, z);
	glEnd();
}

void draw(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	ms3d_render(*gMesh);

	glDisable(GL_TEXTURE_2D);
	line(1, 0, 0);
	line(0, 1, 0);
	line(0, 0, 1);
	glEnable(GL_TEXTURE_2D);

	glutSwapBuffers();
	//glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y) {
	switch(key) {
		case 27: exit(0);
	}
}

int main(int argc, char **argv) {
	int distance = 75.0f;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(800,600);
	glutCreateWindow("Minimal Milkshape Library");
	glutDisplayFunc(draw);
	glutKeyboardFunc(keyboard);
	glClearColor(0.5f, 0.6f, 0.7f, 1.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glm::mat4 projection = glm::perspective(60.0f, 800.0f / 600.0f, 0.01f, 1000.0f);
	glMultMatrixf(glm::value_ptr(projection));
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glm::mat4 modelView = glm::lookAt(glm::vec3(-distance,distance,-distance), glm::vec3(0), glm::vec3(0,1,0));
	glMultMatrixf(glm::value_ptr(modelView));
	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_TEXTURE_2D);

	glShadeModel(GL_SMOOTH);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	GLfloat light_ambient[] = {0.5, 0.5, 0.5, 1.0};
	GLfloat light_diffuse[] = {1.0, 0.0, 0.0, 1.0};
	GLfloat light_specular[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat light_position[] = {-1.0, 1.0, -1.0, 0.0};
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	ms3d_loadtexture = LoadTexture;

	if (ms3d_load("knight_low.ms3d", &gMesh, &gAnimation))
		glutMainLoop();
	else
		std::cout << "Could not initialize.\n";
	return 0;
}
#endif


#endif